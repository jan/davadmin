/*
 * Directory handling JavaScript code.
 *
 * @author Jan Dittberner <jan@dittberner.info>
 * @version $Id$
 * @license GPL
 * @package DAVAdmin
 *
 * Copyright (c) 2007, 2008 Jan Dittberner
 *
 * This file is part of DAVAdmin.
 *
 * DAVAdmin is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAVAdmin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DAVAdmin; if not, see <http://www.gnu.org/licenses/>.
 */

function updateDirectories(xmldata) {
    var dirname = $('dirname', xmldata).text();
    var groups = $('groups', xmldata).text();
    var filecount = $('filecount', xmldata).text();
    var filesize = $('filesize', xmldata).text();
    var maydelete = $('maydelete', xmldata).text();
    var htmltext = '<td>' + dirname + '</td><td>' + groups + '</td><td>' + filecount + ', ' + filesize + '</td><td><a id="edit' + dirname + '" class="editlink" href="#" title="' + intl.translate("Edit this directory's group assignments.") + '"><img class="actionicon" src="images/groups.png" width="16" height="16" alt="' + intl.translate("groups") + '"/></a>';
    if (maydelete == '1') {
        htmltext = htmltext + '<a id="delete' + dirname + '" class="deletelink" href="#" title="' + intl.translate("Delete this directory.") + '"><img class="actionicon" src="images/delete.png" width="16" height="16" alt="' + intl.translate("delete") + '" /></a>';
    }
    htmltext = htmltext + '</td>';
    $('#dirtable').find('tr#dir' + dirname).empty().append(htmltext);
    if (!($('#dirtable').find('tr#dir' + dirname).size())) {
        var rows = $('#dirtable').find('tr');
        var inserted = false;
        for (var i = 0; !inserted && i < rows.length; i++) {
            if ($(rows[i]).find('td:first').text() > dirname) {
                $(rows[i]).before(
                    '<tr id="dir' + dirname + '">' + htmltext + '</tr>');
                inserted = true;
            }
        }
        if (!inserted) {
            $(rows[rows.length-1]).before(
                '<tr id="dir' + dirname + '">' + htmltext + '</tr>');
        }
    }
    $('#dirtable').find('tr#dir' + dirname).find('a.editlink').click(function() {
        editdirectory(this.id.substr(4));
    });
    $('#dirtable').find('tr#dir' + dirname).find('a.deletelink').click(function() {
        removedirectory(this.id.substr(6));
    });
}

function getDirectoryForm(title, dirname, groups) {
    return '<form action="#" id="dirform"><fieldset class="dynaform"><legend><img id="closer" src="images/x.png" width="16" height="16" alt="' + intl.translate("close") + '" /> ' + title + '</legend><div class="formelement"><label for="dirname">' + intl.translate("Directory name:") + '</label><input type="text" name="dirname" id="input-dirname" value="' + dirname + '" /></div><div class="formelement"><label for="groups">' + intl.translate("Groups:") + '</label><input type="text" name="groups" class="form-autocomplete" id="input-groups" value="' + groups + '" /><input type="hidden" class="autocomplete" id="input-groups-autocomplete" value="getgroups.php" /></div><div class="formactions"><input type="submit" name="submit" value="' + intl.translate("Submit") + '" /></div></div></fieldset></form>';
}

function displaydirectoryeditor(title, dirname, groups) {
    $('#content').hide();
    $('#direditor').hide().empty().append(getDirectoryForm(title, dirname,
                                                           groups)).show();
    DAV.autocompleteAutoAttach();
    $('#dirform').find('#input-dirname').focus();
    $('#closer').click(function() {
        $('#direditor').hide().empty();
        $('#content').show();
    });
    $('#dirform').submit(function() {
        if (!this.dirname.value.match(/^[a-zA-Z0-9 -_.]+$/)) {
            alert(intl.translate("Invalid directory name."));
            this.dirname.focus();
            return false;
        }
        $.post(
            "/davadmin/directories.php",
            {method  : 'submitdirectory',
             dirname : this.dirname.value,
             groups  : this.groups.value},
            function(retval) {
                $('div#direditor').hide().empty();
                if (!handleerror(retval)) {
                    updateDirectories(retval);
                    $('#content').show();
                }
            });
        return false;
    });
}

function editdirectory(dirname) {
    $.get(
        "directories.php",
        {dirname : dirname,
         method  : 'getdirectorydata'},
        function(retval) {
            if (!handleerror(retval)) {
                var dirname, groups;
                dirname = $("dirname:first", retval).text();
                groups = $("groups:first", retval).text();
                displaydirectoryeditor(intl.translate("Edit directory data"),
                                       dirname, groups);
            }
        });
}

function newdirectory() {
    displaydirectoryeditor(intl.translate("Create new directory"), '', '');
}

function deletedirectorydialog(dirname) {
    $("#direditor").hide().empty();
    var msg = intl.translate("Should the directory %s really get deleted?",
        [dirname]);
    if (confirm(msg) == true) {
        $.post(
            "directories.php",
            {method  : 'deletedirectory',
             dirname : dirname},
            function(retval) {
                if (!handleerror(retval)) {
                    var dirname = $('dirname:first', retval).text();
                    $('#dirtable').find('tr#dir' + dirname).remove();
                }
            });
    }
}

function removedirectory(dirname) {
    $.get(
        "directories.php",
        {dirname : dirname,
         method  : 'getdirectorydata'},
        function(retval) {
            if (!handleerror(retval)) {
                var username, lastname, firstname;
                var dirname, groups;
                dirname = $("dirname:first", retval).text();
                deletedirectorydialog(dirname);
            }
        });
}

$(function() {
    $("a.editlink").each(function(i) {
        $(this).click(function() { editdirectory(this.id.substr(4)); });
    });
    $("a.newlink").each(function(i) {
        $(this).click(function() { newdirectory(); });
    });
    $("a.deletelink").each(function(i) {
        $(this).click(function() { removedirectory(this.id.substr(6)); });
    });
});
