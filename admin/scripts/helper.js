/**
 * Helper code. Ideas from Drupal (http://drupal.org/).
 *
 * @author Jan Dittberner <jan@dittberner.info>
 * @version $Id$
 * @license GPL
 * @package DAVAdmin
 *
 * Copyright (c) 2007, 2008 Jan Dittberner
 *
 * This file is part of DAVAdmin.
 *
 * DAVAdmin is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAVAdmin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DAVAdmin; if not, see <http://www.gnu.org/licenses/>.
 */

var DAV = DAV || {};

/**
 * Parse a JSON response.
 *
 * The result is either the JSON object, or an object with 'status' 0 and
 * 'data' an error message.
 */
DAV.parseJson = function (data) {
  if ((data.substring(0, 1) != '{') && (data.substring(0, 1) != '[')) {
    return { status: 0, data: data.length ? data : 'Unspecified error' };
  }
  return eval('(' + data + ');');
};

/**
 * Wrapper to address the mod_rewrite url encoding bug.
 */
DAV.encodeURIComponent = function (item, uri) {
  uri = uri || location.href;
  item = encodeURIComponent(item).replace('%2F', '/');
  return uri.indexOf('?q=') ? item : item.replace('%26', '%2526').replace('%23', '%2523');
};

function handleerror(xmldata) {
    if ($('error', xmldata).size()) {
        var msg = intl.translate("An error occured:\n");
        $('error', xmldata).find('errormsg').each(function(i) {
            msg += $(this).text();
        });
        alert(msg);
        return true;
    }
    return false;
    $('#content').show();
}
