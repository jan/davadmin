/*
 * User handling JavaScript code.
 *
 * @author Jan Dittberner <jan@dittberner.info>
 * @version $Id$
 * @license GPL
 * @package DAVAdmin
 *
 * Copyright (c) 2007, 2008 Jan Dittberner
 *
 * This file is part of DAVAdmin.
 *
 * DAVAdmin is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAVAdmin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DAVAdmin; if not, see <http://www.gnu.org/licenses/>.
 */

function updateusers(xmldata) {
    var uid = $(xmldata).find('uid').text();
    var username = $(xmldata).find('username').text();
    var firstname = $(xmldata).find('firstname').text();
    var lastname = $(xmldata).find('lastname').text();
    var loggedin = $(xmldata).find('loggedin').text();
    var htmltext = '<td>' + username + '</td><td>' + lastname + ', ' + firstname + '</td><td><a id="edit' + uid + '" class="editlink" href="#" title="' + intl.translate("Edit this user's data.") + '"><img class="actionicon" src="images/edit.png" width="16" height="16" alt="' + intl.translate("edit") + '"/></a>';
    if (loggedin == '0') {
        htmltext = htmltext + '<a id="delete' + uid + '" class="deletelink" href="#" title="' + intl.translate("Delete this user's data.") + '"><img class="actionicon" src="images/delete.png" width="16" height="16" alt="' + intl.translate("delete") + '" /></a>';
    }
    htmltext = htmltext + '</td>';
    $('#usertable').find('tr#uid' + uid).empty().append(htmltext);
    if (!($('#usertable').find('tr#uid' + uid).size())) {
        var rows = $('#usertable').find('tr');
        var inserted = false;
        for (var i = 0; !inserted && i < rows.length; i++) {
            if ($(rows[i]).find('td:first').text() > username) {
                $(rows[i]).before(
                    '<tr id="uid' + uid + '">' + htmltext + '</tr>');
                inserted = true;
            }
        }
        if (!inserted) {
            $(rows[rows.length-1]).before(
                '<tr id="uid' + uid + '">' + htmltext + '</tr>');
        }
    }
    $('#usertable').find('tr#uid' + uid).find('a.editlink').click(function() {
        edituser(this.id.substr(4));
    });
    $('#usertable').find('tr#uid' + uid).find('a.deletelink').click(function() {
        deleteuser(this.id.substr(6));
    });
    $('#content').show();
}

function getEditUserForm(title, username, firstname, lastname, groups, userid) {
    var retval;
    retval = '<form action="#" id="userform"><fieldset class="dynaform"><legend><img id="closer" src="images/x.png" width="16" height="16" alt="' + intl.translate("close") + '" /> ' + title + '</legend><div class="formelement"><label for="input-username">' + intl.translate("User name:") + '</label><input id="input-username" type="text" name="username" value="' + username + '" ';
    if (userid != null) {
        retval = retval + ' readonly="readonly"';
    }
    retval = retval + '/></div><div class="formelement"><label for="input-firstname">' + intl.translate("First name:") + '</label><input id="input-firstname" type="text" name="firstname" value="' + firstname + '" /></div><div class="formelement"><label for="input-lastname">' + intl.translate("Last name:") + '</label><input id="input-lastname" type="text" name="lastname" value="' + lastname + '" /></div><div class="formelement"><label for="pwd1">' + intl.translate("Password:") + '</label><input type="password" name="pwd1" /></div><div class="formelement"><label for="pwd2">' + intl.translate("Confirmation:") + '</label><input type="password" name="pwd2" /></div><div class="formelement"><label for="groups">' + intl.translate("Groups:") + '</label><input type="text" name="groups" class="form-autocomplete" id="input-groups" value="' + groups + '" /><input type="hidden" class="autocomplete" id="input-groups-autocomplete" value="getgroups.php" /></div><div class="formactions"><input type="submit" name="submit" value="' + intl.translate("Submit") + '" /></div></fieldset></form>';
    return retval;
}

function displayusereditor(title, userid, username, firstname, lastname, groups) {
    $('#content').hide();
    $('#usereditor').hide().empty().append(getEditUserForm(title, username, firstname, lastname, groups, userid)).show();
    DAV.autocompleteAutoAttach();
    $('#closer').click(function() {
        $('#usereditor').hide().empty();
        $('#content').show();
    });
    $('#userform').find('#input-username').focus();
    $('#userform').submit(function() {
        var params;
        if (userid == null) {
            if (!this.username.value.match(/^[a-zA-Z0-9]{2,}$/)) {
                alert(intl.translate("The user name must consist of at least 2 letters or digits and no other characters!"));
                this.username.focus();
                return false;
            }
        }
        if (userid == null || this.pwd1.value.length > 0) {
            if (this.pwd1.value.length < 8) {
                alert(intl.translate("The password must have a lenght of at least 8 characters!"));
                this.pwd1.focus();
                return false;
            }
            if (this.pwd1.value != this.pwd2.value) {
                alert(intl.translate("Password and confirmation have to match!"));
                this.pwd2.focus();
                return false;
            }
        }
        if (!this.groups.value.match(/^[0-9a-zA-z]+(\s*,\s*[0-9a-zA-z]+)*$/)) {
            alert(intl.translate("The groups field has to be a comma separated list of group names which must consist of letters or digits!"));
            this.groups.focus();
            return false;
        }
        if (userid == null) {
            params = {method    : 'submituser',
                      username  : this.username.value,
                      firstname : this.firstname.value,
                      lastname  : this.lastname.value,
                      password  : this.pwd1.value,
                      groups    : this.groups.value};
        } else {
            params = {method    : 'submituser',
                      uid       : userid,
                      username  : this.username.value,
                      firstname : this.firstname.value,
                      lastname  : this.lastname.value,
                      password  : this.pwd1.value,
                      groups    : this.groups.value};
        }
        $.post(
            "users.php", params,
            function(retval) {
                $('div#usereditor').hide().empty();
                if (!handleerror(retval)) {
                    updateusers(retval);
                }
            });
        return false;
    });
}

function deleteuserdialog(userid, username, firstname, lastname) {
    $("#usereditor").hide().empty();
    var msg = intl.translate("Should the user %s %s with the user name %s really get deleted?", [firstname, lastname, username]);
    if (confirm(msg) == true) {
        $.post(
            "users.php",
            {method : 'deleteuser',
             uid : userid},
            function(retval) {
                if (!handleerror(retval)) {
                    var deluid = $('uid:first', retval).text();
                    $('#usertable').find('tr#uid' + deluid).remove();
                }
            });
    }
}

function deleteuser(userid) {
    $.get(
        "users.php",
        {uid    : userid,
         method : 'getuserdata'},
        function(retval) {
            if (!handleerror(retval)) {
                var username, lastname, firstname;
                username = $("username:first", retval).text();
                lastname = $("lastname:first", retval).text();
                firstname = $("firstname:first", retval).text();
                deleteuserdialog(userid, username, firstname, lastname);
            }
        });
}

function edituser(userid) {
    $.get(
        "users.php",
        {uid    : userid,
         method : 'getuserdata'},
        function(retval) {
            if (!handleerror(retval)) {
                var username, lastname, firstname, groups;
                username = $("username:first", retval).text();
                lastname = $("lastname:first", retval).text();
                firstname = $("firstname:first", retval).text();
                groups = $("groups:first", retval).text();
                displayusereditor(intl.translate("Edit user data"),
                                  userid, username, firstname, lastname,
                                  groups);
            }
        });
}

function newuser() {
    displayusereditor(intl.translate("Create new user"), null, '', '', '', '');
}

$(function() {
    $("a.editlink").each(function(i) {
        $(this).click(function() { edituser(this.id.substr(4)); });
    });
    $("a.newlink").each(function(i) {
        $(this).click(function() { newuser(); });
    });
    $("a.deletelink").each(function(i) {
        $(this).click(function() { deleteuser(this.id.substr(6)); });
    });
});
