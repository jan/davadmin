<?php
/**
 * JavaScript internationalization code for DAVAdmin.
 *
 * @author Jan Dittberner <jan@dittberner.info>
 * @version $Id$
 * @license GPL
 * @package DAVAdmin::JavaScript
 *
 * Copyright (c) 2007, 2008 Jan Dittberner
 *
 * This file is part of DAVAdmin.
 *
 * DAVAdmin is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAVAdmin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DAVAdmin; if not, see <http://www.gnu.org/licenses/>.
 */

/** Include the internationalization code. */
require_once(".." . DIRECTORY_SEPARATOR . "i18n.inc.php");

$translatable = file(realpath(implode(DIRECTORY_SEPARATOR,
                                      array(dirname(__FILE__), "..", "..",
                                            "po", "transjs.txt"))));
header("Content-Type: application/x-javascript;charset=UTF-8");
if (is_array($translatable)) {
  print "i18nDict = ";
  $transmap = array();
  foreach ($translatable as $sentence) {
    $sentence = stripcslashes(stripcslashes(trim($sentence)));
    $transmap[$sentence] = _($sentence);
  }
  print json_encode($transmap);
  print ";";
} else {
  print "i18nDict = {};";
}

print("\n\nvar intl = new i18n(i18nDict);");
?>