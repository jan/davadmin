<?php
/**
 * AJAX autocompletion code for group names.
 *
 * @author Jan Dittberner <jan@dittberner.info>
 * @version $Id$
 * @license GPL
 * @package DAVAdmin
 *
 * Copyright (c) 2007, 2008 Jan Dittberner
 *
 * This file is part of DAVAdmin.
 *
 * DAVAdmin is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAVAdmin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DAVAdmin; if not, see <http://www.gnu.org/licenses/>.
 */

/** Include the code shared between normal pages and AJAX. */
require_once("shared.inc.php");

// output is plain text (JSON or an error message)
header("Content-Type: text/plain; charset=UTF-8");

/**
 * Gets group names for autocompletion.
 *
 * @param string $part Comma separated list of groups and beginning of
 * a new group entry
 * @return list of Comma separated lists of groups
 */
function getGroups($part) {
  $regexp = '%(?:^|,\ *)("(?>[^"]*)(?>""[^"]* )*"|(?: [^",]*))%x';
  preg_match_all($regexp, $part, $matches);
  $array = $matches[1];
  $last_string = trim(array_pop($array));
  $retval = array();
  if ($last_string != '') {
    $prefix = count($array) ? implode(",", $array) . ", ": '';
    $groups = file($GLOBALS['davconfig']['group.file']);
    foreach ($groups as $line) {
      list($group, $users) = explode(":", $line);
      $group = trim($group);
      if (!in_array($group, $array) &&
          (stripos($group, $last_string) === 0)) {
        $retval[$prefix . $group] = $group;
      }
    }
  }
  return json_encode($retval);
}

// split group list part from requested URL.
$parts = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], "/") + 1);
print getGroups($parts);
?>