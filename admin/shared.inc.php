<?php
/**
 * Code shared between normal pages and AJAX for DAVAdmin.
 *
 * @author Jan Dittberner <jan@dittberner.info>
 * @version $Id$
 * @license GPL
 * @package DAVAdmin
 *
 * Copyright (c) 2007, 2008 Jan Dittberner
 *
 * This file is part of DAVAdmin.
 *
 * DAVAdmin is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAVAdmin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DAVAdmin; if not, see <http://www.gnu.org/licenses/>.
 */

/** Include common internationalization code. */
require_once("i18n.inc.php");

/**
 * Create a server error with HTTP status code 500 and end script
 * execution.
 *
 * @param string $message error message
 */
function _server_error($message) {
  header('HTTP/1.0 500 Internal Server Error');
  header('Status: 500 Internal Server Error');
  header('Content-Type: text/plain;charset=utf8');
  print($message);
  exit();
}

if (!isset($_SERVER['DavAdminConfDir'])) {
  _server_error(_("The Server is not configured correctly. Please tell your Administrator to set the DavAdminConfDir environment variable."));
}

$confname = $_SERVER['DavAdminConfDir'] . DIRECTORY_SEPARATOR .
'config.inc.php';

if (!file_exists($confname)) {
  _server_error(sprintf(_("The server configuration file '%s' doesn't exist. Please create the file with correct settings."), $confname));
}

/** Include configuration information. */
require_once($confname);
?>