<?php
/**
 * Common code for DAVAdmin.
 *
 * @author Jan Dittberner <jan@dittberner.info>
 * @version $Id$
 * @license GPL
 * @package DAVAdmin
 *
 * Copyright (c) 2007, 2008 Jan Dittberner
 *
 * This file is part of DAVAdmin.
 *
 * DAVAdmin is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAVAdmin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DAVAdmin; if not, see <http://www.gnu.org/licenses/>.
 */

/** Include the code shared between normal pages and AJAX. */
require_once("shared.inc.php");

/** DAV administrator group name. */
define(ADMIN_GROUP, 'davadmin');

/** Include the Smarty template engine. */
require_once("smarty/libs/Smarty.class.php");

/** Global Smarty template engine instance. */
$smarty = new Smarty();

// negotiate the correct template_dir
if (is_dir(($dir = realpath(implode(DIRECTORY_SEPARATOR,
                                    array($smarty->template_dir,
                                          $_SESSION["language"])))))) {
  $smarty->template_dir = $dir;
} else if (is_dir(($dir = realpath(implode(DIRECTORY_SEPARATOR,
                                           array($smarty->template_dir,
                                                 substr($_SESSION["language"],
                                                        0, 2))))))) {
  $smarty->template_dir = $dir;
} else {
  _server_error(sprintf(_("Could not find one of the language directories!\n%s,\n%s"),
                        implode(DIRECTORY_SEPARATOR,
                                array($smarty->template_dir,
                                      $_SESSION["language"])),
                        implode(DIRECTORY_SEPARATOR,
                                array($smarty->template_dir,
                                      substr($_SESSION["language"], 0, 2)))));
}
$compile_dir = implode(DIRECTORY_SEPARATOR, array($davconfig['compile_dir'],
                                                  $_SESSION["language"]));
if (!is_dir($compile_dir)) {
  mkdir($compile_dir);
}
$smarty->compile_dir = $compile_dir;

/**
 * Handle invalid requests to the application. Execution stops here.
 */
function invalidCall() {
  header("Content-Type: text/plain; charset=UTF-8");
  print(_("Invalid call!"));
  die();
}

/**
 * Handle errors with an XML message. Execution stops here.
 *
 * @param string $errormsg error message
 */
function errorAsXml($errormsg) {
  header("Content-Type: text/xml; charset=UTF-8");
  $GLOBALS['smarty']->assign("errormsg", $errormsg);
  $GLOBALS['smarty']->display("error.xml");  
  die();
}

/**
 * Handle errors with an HTML error page. Execution stops here.
 *
 * @param string $errormsg error msg
 */
function errorAsHtml($errormsg) {
  header("Content-Type: text/html; charset=UTF-8");
  $GLOBALS['smarty']->assign("errormsg", $errormsg);
  $GLOBALS['smarty']->display("error.html");  
  die();
}

/**
 * Get the full path of relative directory name.
 *
 * @param string $dirname directory name relative to configured
 * $davconfig['dav.dir']
 * @return string fully qualified directory name
 */
function getFullPath($dirname) {
  return $GLOBALS['davconfig']['dav.dir'] . DIRECTORY_SEPARATOR . $dirname;
}

// check configuration
$errmsgs = array();
if (!isset($davconfig['digest.file'])) {
  array_push($errmsgs,
             sprintf(_("%s is not defined."), "digest.file"));
} elseif (!is_readable($davconfig['digest.file']) ||
          !is_writable($davconfig['digest.file'])) {
  array_push($errmsgs,
             _("The specified digest file is not readable and writable."));
}
if (!isset($davconfig['group.file'])) {
  array_push($errmsgs,
             sprintf(_("%s is not defined."), "group.file"));
} elseif (!is_readable($davconfig['group.file']) ||
          !is_writable($davconfig['group.file'])) {
  array_push($errmsgs,
             _("The specified group file is not readable and writable."));
}
if (!isset($davconfig['namemap.file'])) {
  array_push($errmsgs,
             sprintf(_("%s is not defined."), "namemap.file"));
} elseif (!is_readable($davconfig['namemap.file']) ||
          !is_writable($davconfig['namemap.file'])) {
  array_push($errmsgs,
             _("The specified name mapping file is not readable and writable."));
}
if (!isset($davconfig['dav.dir'])) {
  array_push($errmsgs,
             sprintf(_("%s is not defined."), "dav.dir"));
} elseif (!is_dir($davconfig['dav.dir']) ||
          !is_readable($davconfig['dav.dir']) ||
          !is_writable($davconfig['dav.dir'])) {
  array_push($errmsgs,
             _("The specified DAV directory is no directory or not accessable."));
}
if (empty($davconfig['dav.realm'])) {
  array_push($errmsgs,
             sprintf(_("%s is not defined."), "dav.realm"));
}
if (empty($davconfig['dav.uri'])) {
  array_push($errmsgs,
             sprintf(_("%s is not defined."), "dav.uri"));
}
if (!empty($errmsgs)) {
  errorAsHtml(implode("<br />", $errmsgs));
}

/**
 * Compares two associative arrays of userdata by their username value.
 *
 * @param array &$user1 user data one
 * @param array &$user2 user data two
 * @return value of strcmp of the username values
 */
function cmp_by_usernames(&$user1, &$user2) {
  return strcmp($user1['username'], $user2['username']);
}

$namemapdata = file_get_contents($davconfig['namemap.file']);
$namemap = json_decode($namemapdata, true);
if (is_array($namemap)) {
  uasort($namemap, "cmp_by_usernames");
}
?>
