# Makefile for DAVAdmin
#
# @author Jan Dittberner <jan@dittberner.info>
# @version $Id$
# @license GPL
# @package DAVAdmin
#
# Copyright (c) 2007, 2008 Jan Dittberner
#
# This file is part of DAVAdmin.
#
# DAVAdmin is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# DAVAdmin is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with DAVAdmin; if not, see <http://www.gnu.org/licenses/>.
#
VERSION := 0.3
PROJECT := davadmin
SRCFILES := $(wildcard admin/*.php)
SRCFILES += $(wildcard admin/scripts/*.php)
JSFILES := admin/scripts/autocomplete.js admin/scripts/directories.js \
  admin/scripts/users.js
APISRC := $(shell echo $(SRCFILES) | sed 's/ /,/g' )
TEMPDIR := $(shell mktemp -t -d davadmin.XXXXXXXXXX)
XSRCFILES := $(patsubst %,$(TEMPDIR)/%,$(SRCFILES))
XJSFILES := $(patsubst %,$(TEMPDIR)/%,$(JSFILES))
TRANSLANG := de
POSRC := po
POFILES := $(foreach lang,$(TRANSLANG),$(POSRC)/$(lang)/LC_MESSAGES/$(PROJECT).po)
JSPOFILES := $(foreach lang,$(TRANSLANG),$(POSRC)/$(lang)/LC_MESSAGES/$(PROJECT)js.po)
MOFILES := $(patsubst %.po,%.mo,$(POFILES))
JSMOFILES := $(patsubst %.po,%.mo,$(JSPOFILES))
POT=$(POSRC)/$(PROJECT).pot
JSPOT=$(POSRC)/$(PROJECT)js.pot
JSTRANS=$(POSRC)/transjs.txt

.PHONY: all
all: $(POT) $(JSTRANS) $(MOFILES) $(TEMPDIR)/delete

$(MOFILES): %.mo: %.po
	@echo "msgfmt: $@"
	msgfmt -o$@ $<

$(POFILES): %: $(POT)
	@echo "msgmerge: $@"
	podir=`dirname $@` ; \
	if test ! -d $$podir; then mkdir -p $$podir; fi
	if test -f $@; then \
	  msgmerge -U $@ $(POT); \
	else \
	  cp $(POT) $@; \
	fi
	touch $@

$(POT): $(XSRCFILES) $(XJSFILES) po/pot.sed
	@echo "xgettext: $@"
	cd $(TEMPDIR) && \
	xgettext --default-domain=$(PROJECT) --language=php \
	  --from-code=UTF-8 \
	  --msgid-bugs-address="jan@dittberner.info" -o $(CURDIR)/$@ $(SRCFILES)
	xgettext --default-domain=$(PROJECT) --language=Java \
	  --from-code=UTF-8 --keyword="intl.translate" \
	  --join-existing \
	  --msgid-bugs-address="jan@dittberner.info" -o $(CURDIR)/$@ $(JSFILES)
	sed -f $(CURDIR)/po/pot.sed < $(CURDIR)/$@ > $(CURDIR)/$@.tmp && \
	rm -f $(CURDIR)/$@ && mv $(CURDIR)/$@.tmp $(CURDIR)/$@
	for pofile in $(POFILES); do \
	  if test ! -f $(CURDIR)/$${pofile}; then \
	    cp $(CURDIR)/$@ $(CURDIR)/$${pofile}; \
	  fi; \
	done

$(JSTRANS): $(XJSFILES)
	@echo "generate table of translatable string: $@"
	cd $(TEMPDIR) && \
	xgettext --default-domain=$(PROJECT)js --language=Java \
	  --from-code=UTF-8 \
	  --omit-header --keyword="intl.translate" --properties-output \
	  --no-location -o- $(JSFILES) \
	| awk '/^!.+=$$/ { print }' \
	| sed 's,.\(.*\)=,\1,g; s,\\=,=,g; s,\\ , ,g' \
	> $(CURDIR)/$@

.INTERMEDIATE: $(XSRCFILES) $(XJSFILES)
$(XSRCFILES) $(XJSFILES): $(TEMPDIR)/%: %
	@echo "processing: $<"
	mkdir -p $(@D)
	cp $< $@

$(TEMPDIR)/delete:
	rm -rf $(TEMPDIR)

.PHONY: apidoc
apidoc:
	if [ -d apidoc ]; then rm -r apidoc; fi
	phpdoc -ue on -f $(APISRC) -t apidoc -s

.PHONY: clean distclean
clean: $(TEMPDIR)/delete
	find -name '*~' -type f -exec rm {} \;

distclean: clean
	if [ -d apidoc ]; then rm -r apidoc; fi

RELPREFIX := $(PROJECT)-$(VERSION)
dist: distclean
	cd .. ; if test ! -d $(RELPREFIX); then \
	  ln -s trunk $(RELPREFIX)-dev; \
		tar czh --exclude=.svn -f $(RELPREFIX)-dev.tar.gz $(RELPREFIX)-dev; \
		rm -f $(RELPREFIX)-dev; \
	else \
	  tar czhf $(PROJECT)-$(VERSION).tar.gz $(PROJECT)-$(VERSION); \
	fi
