��    0      �  C         (  	   )     3     F     b  6   p     �     �     �     �     �            (   /     X     o     ~  i   �     �     �     
     %     =  
   P  (   [  ,   �  	   �  +   �  ?   �     '  w   .  i   �  9     _   J  >   �  7   �  6   !	  =   X	  S   �	  F   �	     1
     F
  
   \
  U   g
     �
     �
     �
     �
  N  �
     %     .  &   F     m  4   {     �     �      �       "        B     S  8   o  $   �     �     �  �   �     w     �      �     �     �  	   �  2   �  1   )  	   [  2   e  G   �     �  �   �  {   k  1   �  s     @   �  <   �  <     F   H  n   �  \   �  )   [     �     �  f   �  
          
   #     .                                 (                     ,                        !       '   %                 *             -   
          	      #   +                "      /      &   .       $                                )      0        %d kBytes %s is not defined. A HTTP error %s occured.
%s Confirmation: Could not find one of the language directories!
%s,
%s Create new directory Create new user Delete failed! Delete this directory. Delete this user's data. Directory name: Edit directory data Edit this directory's group assignments. Edit this user's data. Edit user data First name: Groups must be a list of group names separated by commas. Group names must consist of letters and digits. Groups: Invalid call! Invalid directory name %s! Invalid directory name. Invalid user id %s Last name: Password and confirmation have to match! Password must be at least 8 characters long. Password: Should the directory %s really get deleted? Should the user %s %s with the user name %s really get deleted? Submit The Server is not configured correctly. Please tell your Administrator to set the DavAdminConfDir environment variable. The groups field has to be a comma separated list of group names which must consist of letters or digits! The password must have a lenght of at least 8 characters! The server configuration file '%s' doesn't exist. Please create the file with correct settings. The specified DAV directory is no directory or not accessable. The specified digest file is not readable and writable. The specified group file is not readable and writable. The specified name mapping file is not readable and writable. The user name must consist of at least 2 letters or digits and no other characters! There already is a directory entry named %s, but it's not a directory! Uid must be numeric. Unexpected values %s! User name: Username must be at least 2 characters long and must contain letters and digits only. close delete edit groups Project-Id-Version: davadmin 0.2
Report-Msgid-Bugs-To: jan@dittberner.info
POT-Creation-Date: 2008-11-16 18:04+0100
PO-Revision-Date: 2007-12-03 15:28+0100
Last-Translator: Jan Dittberner <jan@dittberner.info>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %d kByte %s ist nicht definiert. Ein HTTP-Fehler %s ist aufgetreten.
%s Bestätigung: Konnte keines der Sprachverzeichnisse finden!
%s,
%s Neues Verzeichnis erstellen Neuen Nutzer anlegen Das Löschen ist fehlgeschlagen! Dieses Verzeichnis löschen. Die Daten dieses Nutzers löschen. Verzeichnisname: Verzeichnisdaten bearbeiten Die Gruppenzuordnungen dieses Verzeichnisses bearbeiten. Die Daten dieses Nutzers bearbeiten. Nutzerdaten bearbeiten Vorname: Im Feld Gruppen muss eine durch Kommata getrennte Liste von Gruppennamen stehen. Gruppennamen dürfen aus Buchstaben und Zahlen bestehen. Gruppen: Ungültiger Aufruf! Ungültiger Verzeichnisname: %s! Ungültiger Verzeichnisname. Ungültige Benutzer-Id %s Nachname: Passwort und Bestätigung müssen übereinstimmen! Das Passwort muss mindestens 8 Zeichen lang sein. Passwort: Soll das Verzeichnis %s wirklich gelöscht werden? Soll der Nutzer %s %s mit dem Nutzernamen %s wirklich gelöscht werden? Absenden Der Server ist nicht richtig konfiguriert. Bitten Sie Ihren Administrator darum, die Umgebungsvariable DavAdminConfDir zu setzen. Das Feld Gruppen muss eine durch Kommata getrennte Liste von Gruppennamen sein, welche aus Buchstaben oder Zahlen bestehen! Das Passwort muss mindestens 8 Zeichen lang sein! Die Serverkonfigurationsdatei '%s' existiert nicht. Bitte legen Sie diese Datei mit den korrekten Einstellungen an. Auf das angegeben DAV-Verzeichnis kann nicht zugegriffen werden. Die angegebene Digest-Datei ist nicht lesbar und schreibbar. Die angegebene Gruppendatei ist nicht lesbar und schreibbar. Die angebene Mapping-Datei für Namen ist nicht lesbar und schreibbar. Der Nutzername muss aus mindestens 2 Buchstaben oder Zahlen bestehen und darf keine anderen Zeichen enthalten! Es gibt bereits einen Verzeichniseintrag mit dem Namen %s, aber dieser ist kein Verzeichnis! Nutzer-Id muss ein numerischer Wert sein. Unerwarteter Wert: %s! Nutzername: Der Nutzername muss mindestens zwei Zeichen lang sein und darf nur aus Buchstaben und Zahlen bestehen. schließen löschen bearbeiten Gruppen 