A HTTP error %s occured.\n%s
Edit this directory's group assignments.
groups
Delete this directory.
delete
close
Directory name\:
Groups\:
Submit
Invalid directory name.
Edit directory data
Create new directory
Should the directory %s really get deleted?
Edit this user's data.
edit
Delete this user's data.
User name\:
First name\:
Last name\:
Password\:
Confirmation\:
The user name must consist of at least 2 letters or digits and no other characters\!
The password must have a lenght of at least 8 characters\!
Password and confirmation have to match\!
The groups field has to be a comma separated list of group names which must consist of letters or digits\!
Should the user %s %s with the user name %s really get deleted?
Edit user data
Create new user
