# Copyright (c) 2007 Jan Dittberner. $Revision$
#
# inspired by Alex Tingle's pot.sed from
# http://blogs.admissions.buffalo.edu/news/wp-content/plugins/eventcalendar3/gettext/pot.sed
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


# Fills in some details in the ec3.pot file.   
1,/^$/ {
  s/SOME DESCRIPTIVE TITLE/DAVAdmin/
  s/YEAR THE PACKAGE.S COPYRIGHT HOLDER/2007, Jan Dittberner/
  s/PACKAGE/davadmin/
  s/FIRST AUTHOR <EMAIL@ADDRESS>, YEAR./Jan Dittberner <jan AT dittberner.info>, 2007./
  s/VERSION/0.2/
  s/CHARSET/UTF-8/
}

