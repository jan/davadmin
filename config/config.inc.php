<?php
/*
 * DavAdmin configuration file.
 *
 * @author Jan Dittberner <jan@dittberner.info>
 * @version $Id$
 * @license GPL
 * @package DAVAdmin
 *
 * Copyright (c) 2007, 2008 Jan Dittberner
 *
 * This file is part of DAVAdmin.
 *
 * DAVAdmin is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAVAdmin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DAVAdmin; if not, see <http://www.gnu.org/licenses/>.
 */

$davconfig = array(
    // Absolute path to template compile dir
    'compile_dir' => '/var/www/templates_c',
    'digest.file' => '/var/www/auth/dav.htdigest',
    'group.file' => '/var/www/auth/dav.groups',
    'namemap.file' => '/var/www/auth/dav.namemap',
    'dav.dir' => '/var/www/html/dav',
    'dav.realm' => 'WebDAV on davhost.example.com',
    'dav.uri' => 'http://davhost.example.com/dav/',
);
?>
